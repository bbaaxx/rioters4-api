import { version } from '../../package.json';
import { Router } from 'express';
import facets from './facets';
import summoner from './summoner';

export default ({ config, db }) => {
	let api = Router();

	// mount the resources
	api.use('/facets', facets({ config, db }));
	api.use('/summoner', summoner({ config, db }));

	// perhaps expose some API metadata at the root
	api.get('/', (req, res) => {
		res.json({ version });
	});

	return api;
}

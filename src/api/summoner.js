import resource from 'resource-router-middleware';
import rp from 'request-promise';
import summoner from '../models/summoner';

const apiConfig = {
  uri: 'https://la1.api.riotgames.com/lol/summoner/v3',
  qs: {
    api_key: 'RGAPI-4750cd83-84b2-44eb-aeaa-e5d52884b934' // -> uri + '?access_token=xxxxx%20xxxxx'
  },
  headers: {
    'User-Agent': 'Request-Promise'
  },
  json: true
}

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'facet',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, name, callback) {
		rp({
			...apiConfig,
			uri: `${apiConfig.uri}/summoners/by-name/${name}`
		}).then(summoner => {
			callback(null, summoner);
		}).catch(err => {
			callback(err);
		});
	},

	/** GET / - List all entities */
	index({ params }, res) {
		res.json(summoner);
	},

	/** POST / - Create a new entity */
	create({ body }, res) {
		body.id = summoner.length.toString(36);
		summoner.push(body);
		res.json(body);
	},

	/** GET /:id - Return a given entity */
	read({ facet }, res) {
		res.json(facet);
	},

	/** PUT /:id - Update a given entity */
	update({ facet, body }, res) {
		for (let key in body) {
			if (key!=='id') {
				facet[key] = body[key];
			}
		}
		res.sendStatus(204);
	},

	/** DELETE /:id - Delete a given entity */
	delete({ facet }, res) {
		summoner.splice(summoner.indexOf(facet), 1);
		res.sendStatus(204);
	}
});

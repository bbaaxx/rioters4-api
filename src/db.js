import loki from 'lokijs';
import config from './config.json';

const ref = new loki('../db/loki.json');
const collections = config.dbCollections.map(collection => ref.addCollection(collection));

export default callback => {
	// connect to a database if needed, then pass it to `callback`:
	callback({ ref, collections });
}
